import os

from .base import *

DEBUG = True
ALLOWED_HOSTS = ['*']

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'NAME': os.getenv('name'),
        'USER': os.getenv('user'),
        'PASSWORD': os.getenv('pass'),
        'HOST': os.getenv('host'),
        'PORT': os.getenv('port')
    }
}

